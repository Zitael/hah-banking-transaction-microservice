package ru.alexandrov.hah.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alexandrov.hah.model.Operation;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
}
