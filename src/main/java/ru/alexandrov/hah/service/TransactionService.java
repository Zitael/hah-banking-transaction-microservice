package ru.alexandrov.hah.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.alexandrov.hah.model.Operation;
import ru.alexandrov.hah.model.Wallet;
import ru.alexandrov.hah.repository.OperationRepository;
import ru.alexandrov.hah.repository.WalletRepository;
import ru.alexandrov.hah.json.request.TransactionRequest;

@Service
@RequiredArgsConstructor
public class TransactionService {
    private final OperationRepository or;
    private final WalletRepository wr;

    public void doTransaction(TransactionRequest request) throws TransactionException {
        Wallet accountFrom = wr.findByAccIdAndCurrency(request.getFromAccId(), request.getCurrencyFrom());
        Wallet accountTo = wr.findByAccIdAndCurrency(request.getFromAccId(), request.getCurrencyTo());
        if (accountFrom == null || accountTo == null) {
            throw new TransactionException("Не найден кошелек");
        }
        Operation operation = makeOperationEntityFromRequest(request);
        if (accountFrom.getAmount() >= request.getAmount()) {
            accountFrom.setAmount(accountFrom.getAmount() - request.getAmount());
            accountTo.setAmount(accountTo.getAmount() + request.getAmount());
        } else throw new TransactionException("Недостаточно средств");

        wr.save(accountFrom);
        wr.save(accountTo);
        or.save(operation);
    }
    private Operation makeOperationEntityFromRequest(TransactionRequest transactionRequest) {
        return new Operation()
                .setAmount(transactionRequest.getAmount())
                .setCurrency(transactionRequest.getCurrencyFrom())
                .setFromAccId(transactionRequest.getFromAccId())
                .setToAccId(transactionRequest.getToAccId());
    }
}
